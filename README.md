# frontend-angularjs


Trabajo a realizar:

* Implementar en Angular la pantalla de Login tal y como se muestra en el zip adjunto.
    * El login debe funcionar correctamente para la organización VISUAL y un usuario prueba, con contraseña prueba. 
        * Si el login es correcto mostrará un mensaje de “Login correcto” como un pop-up.
    * En cualquier otro caso, debe de fallar, mostrando un mensaje bajo el password: “usuario o contraseña incorrecta"
    * La contraseña debe estar oculta salvo que se pulse “mostrar” en cuyo caso pasará a “ocultar” cambiando el icono.
    * Las cadenas de texto deben gestionarse de forma que se puedan traducir fácilmente (i18n o similar)
    * La tipografía:
        * títulos: Montserrat - https://fonts.google.com/specimen/Montserrat
        * cuerpo: Open sans - https://fonts.google.com/specimen/Open+Sans
* La implementación es Material Design, si necesitas saber cómo implementarlo: [https://material.io/develop/web/](https://material.io/develop/web/)
    * Especialmente los campos de texto: [https://material.io/design/components/text-fields.html#implementation](https://material.io/design/components/text-fields.html#implementation)
    * Y los botones: [https://material.io/design/components/buttons.html#implementation](https://material.io/design/components/buttons.html#implementation)
* Subir el código a un repositorio de tu elección y darnos acceso.
* Adjuntar una documentación de cómo ejecutar el proyecto.

 Lo que vamos a valorar es:

* Comportamiento responsive
* Limpieza / Arquitectura de código
* Ser detallista en la implementación
* Gestión del repositorio de código
* BONUS: Testing tanto funcional como de UI. No es necesario, pero si sabes hacerlo, nos gustaría ver tu estilo.
